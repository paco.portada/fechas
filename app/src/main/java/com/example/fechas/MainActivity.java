package com.example.fechas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    TextView informacion;
    DatePicker fecha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        informacion = (TextView) findViewById(R.id.informacion);
        fecha = (DatePicker) findViewById(R.id.fecha);

        String cadena = "2018-11-27";
        int dias = 20;
        Date miFecha;
        Calendar inicio = Calendar.getInstance(Locale.getDefault());
        Calendar fin = Calendar.getInstance(Locale.getDefault());
        int resultado;

        //pasar cadena a fecha usando SimpleDateFormat
        miFecha = cadenaAFecha(cadena);
        informacion.setText("inicio: " + fechaACadena(miFecha));

        //pasar cadena a fecha usando Calendar
        inicio = valoresAFecha(cadena);
        informacion.append("\n" + "inicio: " + inicio.getTime());
        //hay que sumar 1 a inicio.get(Calendar.MONTH)
        informacion.append("\n" + "inicio: " + inicio.get(Calendar.YEAR) + "-" + (inicio.get(Calendar.MONTH) + 1) + "-" + inicio.get(Calendar.DAY_OF_MONTH));

        //sumar días a una fecha
        fin.setTime(inicio.getTime());
        fin.add(Calendar.DATE, dias);
        informacion.append("\n" + "fin: " + fechaACadena(fin.getTime()));
        informacion.append("\n" + "fin: " + fechaACadena(fin));

        //comparar fechas
        resultado = inicio.compareTo(fin);
        if (resultado == 0)
            informacion.append("\n" + "fechas iguales");
        else
        if (resultado > 0)
            informacion.append("\n" + "inicio > fin");
        else
            informacion.append("\n" + "inicio < fin");

        //poner en el DatePicker una fecha
        fecha.updateDate(fin.get(Calendar.YEAR), fin.get(Calendar.MONTH), fin.get(Calendar.DAY_OF_MONTH));
    }

    private Calendar valoresAFecha(String dato) {
        //pasar un string a una fecha
        String[] valores = dato.split("-");
        Calendar c = Calendar.getInstance();
        //hay que restar 1 al mes, porque van de 0 a 11
        c.set(Integer.valueOf(valores[0]), Integer.valueOf(valores[1])-1, Integer.valueOf(valores[2]));
        return c;
    }

    private Date cadenaAFecha(String dato) {
        //pasar un string a una fecha
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date f = null;
        try {
            f = df.parse(dato);
        } catch (ParseException e) {
            e.getMessage();
        }
        return f;
    }

    private String fechaACadena(Date d) {
        //pasar una fecha a un string
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        return df.format(d);
    }

    private String fechaACadena(Calendar c) {
        //pasar una fecha a un string
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        return df.format(c.getTime());
    }
}
